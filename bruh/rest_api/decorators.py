'''
	Defines custom decorators for this app

	A decorator is a function that takes another function as an argument
	and returns a new function.

	Usage :

	@decorator_name
	def to_be_decorated(request)

	What python does is, creates to_be_decorated as a function, calls decorator_name with 
	to_be_decorated as an argument, and the returned value is then bound to the name
	to_be_decorated.

	Creating decorators that take extra arguments is a slightly more involved exercise, in that case,
	instead of just specifying a decorator, you can specify function call that returns the intended 
	decorator. Ex :

	@returns_custom_decorator(argument)
	def to_be_decorated(request)

	Refer to http://passingcuriosity.com/2009/writing-view-decorators-for-django/ for a good tut
'''
from django.conf import settings
from viewHelpers import GetErrorResponse
from auth import GetUserDetailsFromClientToken, ApiAuthError

def JwtAuth(viewFunc):
	def decorated(request, *args, **kwargs):
		if settings.JWT_HEADER_NAME in request.META:
			try:
				request.UserDetails = GetUserDetailsFromClientToken(request.META[settings.JWT_HEADER_NAME])
			except ApiAuthError as e:
				return GetErrorResponse(str(e), 400)

			# Call the viewFunc post decoration and return what it returns
			return viewFunc(request, *args, **kwargs)
		else:
			return GetErrorResponse("Bad request : Client token not provided.", 400)

	return decorated