from django.http import HttpResponse
from django.core.serializers.json import DjangoJSONEncoder
import json

def getError(message, code):
    # Helper method to get the error json object
    return {
        "status" : "failed",
        "code" : code,
        "message" : message
    }

def getJsonHttpResponse(jsonStr, code=200):
    return HttpResponse(jsonStr, content_type="application/json", status=code)

def GetErrorResponse(message, code):
    #Helper method to get error json response
    error = getError(message,code)
    return getJsonHttpResponse(json.dumps(error, cls=DjangoJSONEncoder), code)

def GetSuccessResponse(data, code=200):
    ret = {
        "status" : "OK",
        "code" : code,
        "data" : data
    }
    return getJsonHttpResponse(json.dumps(ret, cls=DjangoJSONEncoder), code)

def ParseDaysOfWeek(postDict):
    daysOfWeekFormData = postDict.getlist("daysOfWeek[]")
    if len(daysOfWeekFormData) < 1:
        daysOfWeekFormData = postDict.getlist("daysOfWeek")            
    daysOfWeekData = [int(d) for d in daysOfWeekFormData]
    # If still daysOfWeek is empty then check if it is coming with indices
    if len(daysOfWeekData) < 1:
        for k in postDict.keys():
            if str(k).startswith("daysOfWeek["):
                daysOfWeekData.append(int(postDict.get(k)))
    return daysOfWeekData