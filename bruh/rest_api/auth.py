'''
	Contains auth related code
'''
from django.conf import settings
from data_model.models import UserDetails
import jwt
import urllib2
import json

class ApiAuthError(Exception):
	def __init__(self, message, code):
		super(ApiAuthError, self).__init__(message)
		self.Code = code

def GetUserDetailsFromClientToken(token):
	try:
		payload = jwt.decode(token, settings.SECRET_KEY)
		try:
			return UserDetails.objects.get(UniqueId = payload["UniqueId"])
		except UserDetails.DoesNotExist:
			raise ApiAuthError("Bad request : Client token doesnt belong to any user", 400)

	except jwt.DecodeError:
		raise ApiAuthError("Bad request : Client token has been tampered with.", 400)

def GetClientTokenForUser(user):
	'Helper method for generating client token from UserDetails'
	if not isinstance(user, UserDetails):
		raise ValueError("Invalid argument, user must be of the type UserDetails")

	return jwt.encode({"UniqueId" : user.UniqueId}, settings.SECRET_KEY)

def GetFacebookAccessToken(fbId):
    #Get an App Access token
    appId = settings.APP_ID
    appSecret = settings.APP_SECRET
    appAccessTokenUrl = "https://graph.facebook.com/oauth/access_token?client_id=%s&client_secret=%s&grant_type=client_credentials" %(appId, appSecret)
    appAccessTokenResponse = urllib2.urlopen(appAccessTokenUrl).read()
    appAccessToken = appAccessTokenResponse.split("=")[1]
    return appAccessToken

def ValidateFacebookAccessToken(fbId, accessToken):
    #Validate the User Access token
    appAccessToken = GetFacebookAccessToken(fbId)
    validateUrl = "https://graph.facebook.com/debug_token?input_token=%s&access_token=%s" %(accessToken, appAccessToken)
    response = json.load(urllib2.urlopen(validateUrl))
    if response["data"]["is_valid"] and response["data"]["user_id"] == fbId:
        return True
    else:
        return False

def GetClientTokenForFbCredentials(fbId, accessToken, fbValidator = ValidateFacebookAccessToken):
	try:
		user = UserDetails.objects.get(FbId = fbId)
	except UserDetails.DoesNotExist:
		raise ApiAuthError("User not found", 404)

	try:
		isValid = fbValidator(fbId, accessToken)
	except Exception:
		raise ApiAuthError("Authentication failed : Unable to authenticate with facebook",500)

	if not isValid:
		raise ApiAuthError("Authentication failed. Invalid user credentials", 401)

	return GetClientTokenForUser(user)