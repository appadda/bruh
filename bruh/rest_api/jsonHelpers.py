from django.db import models
from data_model.modelView import ModelView
import json

def GetSerializableDataForModel(model, ignoredFields = []):
    if (not isinstance(model, models.Model)) and (not isinstance(model, ModelView)):
        raise ValueError("Method can accept only django models and ModelView instances.")
    try:
        serializables = model.GetSerializableMembers()
    except AttributeError:
        return None
    serializables = [f for f in serializables if not f in ignoredFields]

    ret = {}

    # recursively convert the model into json
    for attr in serializables:
        data = getattr(model,attr)
        retAttrName = attr[0].lower()+attr[1:]
        if isinstance(data, models.Model):
            ret[retAttrName] = GetSerializableDataForModel(data)
        else:
            ret[retAttrName] = data

    # Process custom serializables if GetCustomSerializables is defined
    if "GetCustomSerializables" in dir(model):
        customSerializables = model.GetCustomSerializables()
        # Merge the dictionaries
        ret = dict(ret.items() + customSerializables.items())

    return ret

def GetSerializableDataForQuerySet(querySet, ignoredFields = []):
    if not isinstance(querySet, models.query.QuerySet):
        raise ValueError("Method can accept only django queryset.")
    
    retAttrName = querySet.model._meta.verbose_name_plural.title()
    retAttrName = retAttrName[0].lower() + retAttrName[1:]

    ret = {retAttrName:[]}
    if not querySet:
      return ret

    for query in querySet:
        ret[retAttrName].append(GetSerializableDataForModel(query, ignoredFields))

    return ret

def GetModelJson(model):
    if not isinstance(model, models.Model):
        raise ValueError("GetModelJson can accept only django models.")
    return json.dumps(GetSerializableDataForModel(model))

def GetQuerySetJson(querySet):
    if not isinstance(querySet, models.query.QuerySet):
        raise ValueError("GetQuerySetJson can accept only django QuerySets.")
    return json.dumps(GetSerializableDataForQuerySet(querySet))

