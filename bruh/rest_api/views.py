from django.http import HttpResponse
from data_model import models as DataModels
from jsonHelpers import GetSerializableDataForModel, GetSerializableDataForQuerySet
from viewHelpers import GetErrorResponse, GetSuccessResponse, ParseDaysOfWeek
from auth import GetClientTokenForFbCredentials, ApiAuthError, ValidateFacebookAccessToken, GetClientTokenForUser
from django.views.decorators.csrf import csrf_exempt
from decorators import JwtAuth
from modelParser import ParseModelData, ModelParserError
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db import IntegrityError
import uuid

import pdb



@csrf_exempt
def GetClientTokenAndUser(request):
    if request.method != "POST":
        return GetErrorResponse("Bad request : unsupported request method - "+request.method, 400)

    if not 'fbId' in request.POST:
        return GetErrorResponse("Bad request : Credentials missing - fbId", 400)
    if not 'accessToken' in request.POST:
        return GetErrorResponse("Bad request : Credentials missing - accessToken", 400)

    fbId = request.POST["fbId"]
    accessToken = request.POST["accessToken"]

    try:
        token = GetClientTokenForFbCredentials(fbId, accessToken)
    except ApiAuthError as e:
        return GetErrorResponse(str(e), e.Code)
    try:
        user = DataModels.UserDetails.objects.get(FbId = fbId)
    except DataModels.User.DoesNotExist:
        return GetErrorResponse("User not found", 404)
    serializedUser = GetSerializableDataForModel(user)
    responseData = {"user" : serializedUser, "clientToken" : token}
    return GetSuccessResponse(responseData)


@JwtAuth
def GetUserDetails(request):

    if request.method != "GET":
        return GetErrorResponse("Bad request : unsupported request method - "+request.method, 400)
    user = request.UserDetails
    responseData = GetSerializableDataForModel(user)
    return GetSuccessResponse(responseData)

def RegisterUser(request):
    if request.method == "POST":
        try:
            user = ParseModelData(request.POST, DataModels.UserDetails, ["id", "UniqueId"], [])
        except ModelParserError as e:
            return GetErrorResponse("Bad request : Missing mandatory data for registering a user - "+e.FieldName, 400)
        if ValidateFacebookAccessToken(user.FbId, user.AccessToken):
            user.save()
            userData = GetSerializableDataForModel(user)
            responseData = {"clientToken" : GetClientTokenForUser(user), "user" : userData}
            return GetSuccessResponse(responseData, 200)
        else:
            return GetErrorResponse("Bad request : Acess Token validation failed", 400)
    else:
        errorMsg = "Unsupported request method : %s" % request.method
        return GetErrorResponse(errorMsg, 405)


@csrf_exempt
def GetClientTokenFb(request):
    if request.method != "POST":
        return GetErrorResponse("Bad request : unsupported request method - "+request.method, 400)

    if not 'fbId' in request.POST:
        return GetErrorResponse("Bad request : Credentials missing - fbId", 400)
    if not 'accessToken' in request.POST:
        return GetErrorResponse("Bad request : Credentials missing - accessToken", 400)

    fbId = request.POST["fbId"]
    accessToken = request.POST["accessToken"]

    try:
        token = GetClientTokenForFbCredentials(fbId, accessToken)
    except ApiAuthError as e:
        return GetErrorResponse(str(e), e.Code)

    return GetSuccessResponse({"clientToken" : token})



@csrf_exempt
def UserCrud(request):
    if request.method == "GET":
        return GetUserDetails(request)
    elif request.method == "POST":
        return RegisterUser(request)
    else:
        errorMsg = "Unsupported request method : %s" % request.method
        return GetErrorResponse(errorMsg, 405)


def deleteUsers(request):
    DataModels.UserDetails.objects.all().delete()
    return GetSuccessResponse({"status" : "OK"})



