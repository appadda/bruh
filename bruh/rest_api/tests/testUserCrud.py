from django.test import TestCase, client
from django.core.urlresolvers import reverse
from django.conf import settings
from rest_api.auth import GetClientTokenForUser
from data_model.models import UserDetails

class TestUserCrud(TestCase):
    def setUp(self):
        self.client = client.Client(enforce_csrf_checks=True)
        self.user = UserDetails(Name="TestUser", FbId="100002031687931", AccessToken="testAccessToken", 
            Phone="1234567890", Email="test@test.com")
        self.user.save()
        self.token = GetClientTokenForUser(self.user)

    def testInvalidHttpMethods(self):
        url = reverse(settings.REST_VIEWNAMES['User'])
        response = self.client.put(url,  **{settings.JWT_HEADER_NAME : self.token})
        expected = '{ "status" : "failed", "message" : "Unsupported request method : PUT", "code" : 405}'
        self.assertJSONEqual(response.content, expected)
        
        response = self.client.delete(url,  **{settings.JWT_HEADER_NAME : self.token})
        expected = '{ "status" : "failed", "message" : "Unsupported request method : DELETE", "code" : 405}'
        self.assertJSONEqual(response.content, expected)

