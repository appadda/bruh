from django.test import TestCase
from data_model.models import UserDetails
from django.conf import settings
from rest_api.auth import GetUserDetailsFromClientToken, GetClientTokenForFbCredentials, ApiAuthError, GetClientTokenForUser
from rest_api.tests.mocks import MockTrue, MockFalse
import jwt

class AuthTests(TestCase):
    'tests for functions in auth module' 
    def setUp(self):
        self.user = UserDetails(Name="TestUser", FbId="100002031687931", AccessToken="testAccessToken", 
            Phone="1234567890", Email="test@test.com")
        self.user.save()

    def testGetUserDetailsFromClientToken(self):
        token = jwt.encode({"UniqueId" : self.user.UniqueId}, settings.SECRET_KEY)
        u = GetUserDetailsFromClientToken(token)
        self.assertEqual(u.id, self.user.id)
        self.assertEqual(u.UniqueId, self.user.UniqueId)

    def testGetClientTokenForUser(self):
        expected = jwt.encode({"UniqueId" : self.user.UniqueId}, settings.SECRET_KEY)
        token = GetClientTokenForUser(self.user)
        self.assertEqual(expected,token)

    def testGetTokenForUser(self):
        token = GetClientTokenForFbCredentials(self.user.FbId, self.user.AccessToken, MockTrue)
        expected = jwt.encode({"UniqueId":self.user.UniqueId}, settings.SECRET_KEY)
        self.assertEqual(token, expected)

        try:
            token = GetClientTokenForFbCredentials('garbage', 'garbage')
            self.fail("Api auth error must be raised for user not found")
        except ApiAuthError as e:
            self.assertEqual(str(e), "User not found")
            self.assertEqual(e.Code, 404)

        try:
            token = GetClientTokenForFbCredentials(self.user.FbId, 'garbage', MockFalse)
            self.fail("Api auth error must be raised for invalid user credentials")
        except ApiAuthError as e:
            self.assertEqual(str(e), "Authentication failed. Invalid user credentials")
            self.assertEqual(e.Code, 401)

