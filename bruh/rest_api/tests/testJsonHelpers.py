from django.test import TestCase
from data_model.models import UserDetails
from datetime import date, timedelta, time
from rest_api.jsonHelpers import GetSerializableDataForModel, GetModelJson

class JsonHelpersTests(TestCase):
    def setUp(self):
        self.user = UserDetails(Name="TestUser", FbId="100002031687931", AccessToken="testAccessToken", 
            Phone="1234567890", Email="test@test.com")
        self.user.save()
        self.teacher = UserDetails(FbId="1234a2bc", AccessToken="testAcc1essToken", 
            Name="testNdame", Phone="12334567890", Email="test1@test.com")
        self.teacher.save()

    def testGetSerializableDataForQuerysetIgnoredFields(self):
        #TODO : Write a test for nested models with foriegn keys to make sure that the conversion is happening correctly
        # Also try and make this independent of the data models from the data_model app
        data = GetSerializableDataForModel(self.user, ["Email"])
        expected = {"name":"TestUser", "uniqueId" : self.user.UniqueId, "fbId":"100002031687931",
                    "phone":"1234567890"}
        self.assertDictEqual(data,expected)

    def testGetModelJson(self):
        userJson = GetModelJson(self.user)
        uidData = '"uniqueId" : "%s", ' % self.user.UniqueId
        expected = '{"name":"TestUser", '+uidData+'"fbId":"100002031687931", "phone":"1234567890", "email":"test@test.com"}'
        self.assertJSONEqual(expected,userJson)


    # Todo add test involving GetCustomSerializables for a models