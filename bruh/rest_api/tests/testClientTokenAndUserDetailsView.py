from django.test import TestCase, client
from data_model.models import UserDetails
from django.core.urlresolvers import reverse
from datetime import date, timedelta, time
from django.conf import settings
from rest_api.auth import GetClientTokenForUser, GetFacebookAccessToken
import jwt

class ClientTokenAndUserDetailsApiTests(TestCase):
    'Contains test methods for the UserDetails rest API'

    def setUp(self):
        self.user = UserDetails(Name="TestUser", FbId="100002031687931", AccessToken="testAccessToken", 
            Phone="1234567890", Email="test@test.com")
        self.user.save()
        self.client = client.Client(enforce_csrf_checks=True)
        self.maxDiff = None

    def testGetFails(self):
            url = reverse(settings.REST_VIEWNAMES['ClientTokenAndUserDetails'])
            response = self.client.get(url, {'fbId' : self.user.FbId, 'accessToken' : self.user.AccessToken})
            expected = '{ "status" : "failed", "message" : "Bad request : unsupported request method - GET", "code" : 400}'
            self.assertJSONEqual(response.content,expected)
            self.assertEqual(response.status_code, 400)

    #Todo : Fix the tests to use a proper FbId which has given access to our app
#    def testProperRequest(self):
#        url = reverse(settings.REST_VIEWNAMES['ClientTokenAndUserDetails'])
#        accessToken = GetFacebookAccessToken(self.user.FbId)
#        response = self.client.post(url, {'fbId' : self.user.FbId, 'accessToken' : accessToken})
#        expectedToken = GetClientTokenForUser(self.user)
#        uidData = '"uniqueId" : "%s", ' % self.user.UniqueId
#        expected = '{ "status" : "OK", "data" : {userDetails: {"name" : "TestUser", "fbId" : "100002031687931", '+uidData+'"phone" : "1234567890", "email" : "test@test.com"}"clientToken" : "'+expectedToken+'"}, "code" : 200}'
#        self.assertJSONEqual(response.content, expected)

    def testCredentialsNotProvided(self):
        url = reverse(settings.REST_VIEWNAMES['ClientTokenAndUserDetails'])
        response = self.client.post(url, {'fbId' : self.user.FbId})
        expected = '{ "status" : "failed", "message" : "Bad request : Credentials missing - accessToken", "code" : 400}'
        self.assertJSONEqual(response.content,expected)
        self.assertEqual(response.status_code, 400)

        response = self.client.post(url, {'accessToken' : self.user.AccessToken})
        expected = '{ "status" : "failed", "message" : "Bad request : Credentials missing - fbId", "code" : 400}'
        self.assertJSONEqual(response.content,expected)
        self.assertEqual(response.status_code, 400)

    def testUserNotFound(self):
        url = reverse(settings.REST_VIEWNAMES['ClientTokenAndUserDetails'])
        response = self.client.post(url, {'fbId' : self.user.FbId+'garbage', 'accessToken': 'garbage'})
        expected = '{ "status" : "failed", "message" : "User not found", "code" : 404}'
        self.assertJSONEqual(response.content,expected)
        self.assertEqual(response.status_code, 404)

    # Add concept of online and offline tests
    # def testAuthFailure(self):
    #     url = reverse(settings.REST_VIEWNAMES['ClientTokenAndUserDetails])
    #     response = self.client.post(url, {'fbId' : self.user.FbId, 'accessToken': 'garbage'})
    #     expected = '{ "status" : "failed", "message" : "Authentication failed. Invalid user credentials", "code" : 403}'
    #     self.assertJSONEqual(response.content,expected)
    #     self.assertEqual(response.status_code, 403)
