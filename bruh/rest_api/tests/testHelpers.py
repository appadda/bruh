'''
	Utility code for creating tests
'''
class MockObject(object):
    '''
        Class for creating custom mock objects with arbitrary properties
    '''
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)
