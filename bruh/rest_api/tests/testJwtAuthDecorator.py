from django.test import TestCase
from data_model.models import UserDetails
from rest_api.decorators import JwtAuth
from django.conf import settings
from testHelpers import MockObject

import jwt
import json

class JwtAuthDecoratorTests(TestCase):
    'Tests for the JwtAuth decorator'
    def setUp(self):
        self.user = UserDetails(Name="TestUser", FbId="100002031687931", AccessToken="testAccessToken", 
            Phone="1234567890", Email="test@test.com")
        self.user.save()

    def callDecoratedView(self, request, testBed):
        'Helper method to create a decorated view function with appropriate stubs and call it'
        @JwtAuth        
        def testFunc(request, testBed):
            # Takes userDetails from request and sets them on testBed ( to be used for asserting things went well )
            testBed.UserDetails = request.UserDetails
            return "Success"

        return testFunc(request, testBed)

    def testJwtAuthSetsUserDetails(self):
        # create the Jwt Token to be used.
        token = jwt.encode({"UniqueId" : self.user.UniqueId}, settings.SECRET_KEY)
        request = MockObject(META = {settings.JWT_HEADER_NAME : token})
        testBed = MockObject(UserDetails=None)

        self.callDecoratedView(request,testBed)

        self.assertEqual(testBed.UserDetails.UniqueId, self.user.UniqueId)
        self.assertEqual(testBed.UserDetails.id, self.user.id)

    def testJwtAuthUserNotFound(self):
        token = jwt.encode({"UniqueId" : 'invalid'}, settings.SECRET_KEY)
        request = MockObject(META = {settings.JWT_HEADER_NAME : token})
        testBed = MockObject(UserDetails=None)

        response =json.loads(self.callDecoratedView(request,testBed).content)
        self.assertEqual(response["status"], "failed")
        self.assertEqual(response["code"],400)
        self.assertEqual(response["message"],"Bad request : Client token doesnt belong to any user")


    def testJwtAuthInvalidToken(self):
        token = jwt.encode({"UniqueId" : 'invalid'}, settings.SECRET_KEY)+'garbage'
        request = MockObject(META = {settings.JWT_HEADER_NAME : token})
        testBed = MockObject(UserDetails=None)

        response =json.loads(self.callDecoratedView(request,testBed).content)
        self.assertEqual(response["status"], "failed")
        self.assertEqual(response["code"],400)
        self.assertEqual(response["message"],"Bad request : Client token has been tampered with.")

    def testJwtAuthTokenNotSupplied(self):
        request = MockObject(META = {})
        testBed = MockObject(UserDetails=None)

        response =json.loads(self.callDecoratedView(request,testBed).content)
        self.assertEqual(response["status"], "failed")
        self.assertEqual(response["code"],400)
        self.assertEqual(response["message"],"Bad request : Client token not provided.")
