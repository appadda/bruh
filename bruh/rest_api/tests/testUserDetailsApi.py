from django.test import TestCase, client
from data_model.models import UserDetails
from django.core.urlresolvers import reverse
from datetime import date, timedelta, time
from django.conf import settings
import jwt

class UserDetailsApiTests(TestCase):
    'Contains test methods for the UserDetails rest API'

    def setUp(self):
        self.user = UserDetails(Name="TestUser", FbId="100002031687931", AccessToken="testAccessToken", 
            Phone="1234567890", Email="test@test.com")
        self.user.save()
        self.client = client.Client(enforce_csrf_checks=True)
        self.maxDiff = None
        self.teacher = UserDetails(FbId="1234a2bc", AccessToken="testAcc1essToken", 
            Name="testNdame", Phone="12334567890", Email="test1@test.com")
        self.teacher.save()


    def testGetUserByIdPositive(self):
        # test happy case
        url = reverse(settings.REST_VIEWNAMES['User'])
        token = jwt.encode({"UniqueId" : self.user.UniqueId}, settings.SECRET_KEY)
        response = self.client.get(url, **{settings.JWT_HEADER_NAME : token})
        uidData = '"uniqueId" : "%s", ' % self.user.UniqueId
        expected = '{ "status" : "OK", "data" : {"name" : "TestUser", "fbId" : "100002031687931", '+uidData+'"phone" : "1234567890", "email" : "test@test.com"}, "code" : 200}'
        self.assertJSONEqual(response.content, expected)
