from django.core import serializers
from django.db import models
import json
'''
    Code to help parse a model from data received in requests
'''

class ModelParserError(Exception):
    def __init__(self, message, fieldName="", *args, **kwargs):
        super(ModelParserError, self).__init__(message, *args, **kwargs)
        self.FieldName = fieldName

def ParseModelData(data, model, ignoredFields, optional):
    if not issubclass(model, models.Model):
        raise ModelParserError("Parsing error : Please provide a class that subclasses the django.db.Model class")

    fields = [f.name for f in model._meta.fields if not f.name in ignoredFields]
    fieldData = {}    
    
    for field in fields:
        try:
                fieldName = field[0].lower()+field[1:]
                fieldData[field] = data[fieldName]
        except KeyError as e:
            if not fieldName in optional:
                raise ModelParserError("Parsing error : Missing expected fields for model data : " + ",".join(e.args), e.args[0])

    # This format is necessary for the serializer to work
    modelData = {
        "model" : model.__module__.split('.')[0] + '.' + model.__name__.lower(),
        "fields" : fieldData
    }

    # the serializer strictly expects a list of json objects
    jsonData = json.dumps([modelData])

    return serializers.deserialize('json',jsonData).next().object
