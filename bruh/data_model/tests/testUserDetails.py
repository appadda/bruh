from django.test import TestCase
from data_model.models import UserDetails

class UserDetailsTests(TestCase):
    '''
        Tests the UserDetails model.
    '''

    def setUp(self):
        self.userDetails = UserDetails(FbId="1234abc", AccessToken="testAccessToken", 
            Name="testName", Phone="1234567890", Email="test@test.com")
        self.userDetails.save()

    def testUserDetailsProperties(self):
        "Tests that the model has all properties appropriately set"
        userDetails = self.userDetails

        self.assertEqual(userDetails.FbId, "1234abc")
        self.assertEqual(userDetails.AccessToken, "testAccessToken")
        self.assertEqual(userDetails.Name, "testName")
        self.assertEqual(userDetails.Phone, "1234567890")
        self.assertEqual(userDetails.Email, "test@test.com")
        self.assertTrue(type(userDetails.get_absolute_url()) == type('') )
        self.assertTrue(type(userDetails.UniqueId) == type(''))

    def testUserDetailsUniqueFields(self):
        uniqueFields = ['FbId','Phone','Email']
        for field in self.userDetails._meta.fields:
            if field.name in uniqueFields:
                self.assertTrue(field.unique, msg="%s in UserDetails model must be unique" % field.name)

    def testSeriazableMembers(self):
        self.assertListEqual(self.userDetails.GetSerializableMembers(),['UniqueId','FbId', 'Name', 'Phone', 'Email'])

    def tearDown(self):
        del self.userDetails
