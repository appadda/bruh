from django.test import TestCase
from data_model.modelView import ModelView
from data_model.models import UserDetails
from rest_api.jsonHelpers import GetSerializableDataForModel

class ModelViewTests(TestCase):
    def setUp(self):
        self.userDetails = UserDetails(FbId="1234abc", AccessToken="testAccessToken", 
            Name="testName", Phone="1234567890", Email="test@test.com")
        self.userDetails.save()

    def testHappyCase(self):
        m = ModelView(self.userDetails, ["FbId","AccessToken","Phone"])
        self.assertEqual(m.model.id, self.userDetails.id)
        self.assertEqual(m.model.FbId, self.userDetails.FbId)
        self.assertEqual(m.model.UniqueId, self.userDetails.UniqueId)
        self.assertListEqual(m.serializables, ["FbId","AccessToken","Phone"])

    def testInvalidArgument(self):
        try:
            ModelView(3, [""])
            self.fail("ModelView should accept only django models")
        except ValueError as e:
            self.assertEqual(str(e), "ModelView can wrap only django model objects")

    def testInvalidSerializables(self):
        try:
            ModelView(self.userDetails, ["garbage1","garbage12"])
            self.fail("ModelView should only accept valid fields from supplied model in serializables")
        except ValueError as e:
            self.assertEqual(str(e),"ModelView received invalid field names in serializables.")

    def testGetSerializables(self):
        m = ModelView(self.userDetails, ["FbId","AccessToken","Phone"])
        self.assertListEqual(m.GetSerializableMembers(),["FbId","AccessToken","Phone"])
        self.assertEqual(getattr(m,"FbId"),self.userDetails.FbId)
        self.assertEqual(getattr(m,"AccessToken"), self.userDetails.AccessToken)
        self.assertEqual(getattr(m,"Phone"), self.userDetails.Phone)

    def testModelViewJson(self):
        m = ModelView(self.userDetails, ["FbId","AccessToken","Phone"])
        data = GetSerializableDataForModel(m)
        expected = {
            "fbId" : self.userDetails.FbId,
            "accessToken" : self.userDetails.AccessToken,
            "phone" : self.userDetails.Phone
        }
        self.assertDictEqual(data, expected)