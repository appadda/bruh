from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.core.exceptions import ValidationError
from django.conf import settings
from django.core.urlresolvers import reverse

import os
import uuid

class UserDetails(models.Model):
    FbId = models.CharField(max_length=255, unique=True, db_index=True)
    AccessToken = models.CharField(max_length=1000, db_index=True) 
    Name = models.CharField(max_length=255)
    Phone = models.CharField(max_length=40, unique=True)
    Email = models.EmailField(unique=True)
    UniqueId = models.CharField(max_length=100, unique=True, db_index=True)

    class Meta:
      verbose_name_plural = "UsersDetails"

    def get_absolute_url(self):
        'The standard absolute url method for django'
        ret = reverse(settings.REST_VIEWNAMES['User']) 
        return ret

    def GetSerializableMembers(self):
        '''A utility method to aid serializers in knowing the attribs on the class that 
            can be serialized. Not a standard django function - custom to this project'''
        return ['UniqueId','FbId', 'Name', 'Phone', 'Email']

    # Make sure that the model gets a uniqueId when it is saved for the first time.
    def save(self, *args, **kwargs):
        if not self.UniqueId:
            self.UniqueId = str(uuid.uuid4())

        super(UserDetails, self).save(*args, **kwargs)