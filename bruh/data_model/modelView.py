from django.db import models

'''Code for Model Views
    Can be used to create different views for models to expose for json serialization
    can then be passed to GetSerializableDataForModel or GetModelJson
'''


class ModelView(object):
    def __init__(self, model, serializables):
        # Make sure its a model that we have been given
        if not isinstance(model, models.Model):
            raise ValueError("ModelView can wrap only django model objects")

        # make sure the model has the fields specified in serializables
        modelFields = [field.name for field in model._meta.fields]
        for s in serializables:
            if not s in modelFields:
                raise ValueError("ModelView received invalid field names in serializables.")

        self.model = model
        self.serializables = serializables

    def GetSerializableMembers(self):
        return self.serializables

    def __getattr__(self, name):
        return getattr(self.model, name)
