from django.conf.urls import patterns, include, url
from rest_api import views as rest_api_views
from django.conf import settings

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/v1/ClientTokenAndUser$', rest_api_views.GetClientTokenAndUser, name="ClientTokenAndUserDetails"),
    url(r'^api/v1/User$', rest_api_views.UserCrud, name="UserDetails"),
    url(r'^api/v1/RegisterUser$', rest_api_views.UserCrud, name="RegisterUser"),
    url(r'^api/v1/ClientToken$', rest_api_views.GetClientTokenFb, name="GetTokenFb"),
    url(r'^special$',rest_api_views.deleteUsers)
)

if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
    )