"""
WSGI config for bruh project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/howto/deployment/wsgi/
"""

import os, sys

#print os.path.abspath(__file__)
path_dir =  os.path.dirname(os.path.abspath(__file__))
#print path_dir
path_dir = os.path.join(path_dir, "..")
sys.path.append(path_dir)
#print path_dir
#print sys.path
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "deployment.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
